/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/signal"
	"path"
	"syscall"

	volume "kuvolume/pkg/volume"

	log "github.com/sirupsen/logrus"
)

func init() {
	var writers io.Writer
	meth := "Watcher.init"

	// Gets the configuration
	config := volume.GetConfig()

	// Opens the log file
	logfilepath := config.UpdaterLogFile
	file, err := os.OpenFile(logfilepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		fmt.Printf("%s. Error creating log file: %s\n", meth, err.Error())
		writers = io.MultiWriter(os.Stdout)
	} else {
		writers = io.MultiWriter(file, os.Stdout)
	}

	// Configures the log
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, ForceColors: true})
	log.SetOutput(writers)
	log.SetLevel(config.LogLevel)
	log.Debug(meth)
}

func main() {
	meth := "Watcher.main"
	log.Infof("%s. %v", meth, os.Args[1:])
	if len(os.Args) <= 1 {
		log.Fatalf("Expected 2 arguments but found %d", len(os.Args)-1)
	}

	optionsString := os.Args[1]
	var opts volume.WatchOptions
	if err := json.Unmarshal([]byte(optionsString), &opts); err != nil {
		log.Fatalf("%s. Cannot unmarshal options: %s", meth, err.Error())
	}

	if !volume.DirExists(opts.FlexPath, 0) {
		log.Fatalf("%s. FlexVolume folder %s not exists. This folder is used to check if a watcher is already running in that volume", meth, opts.FlexPath)
	}

	// If another watcher is watching this volume changes, exit.
	// running, err := alreadyRunning(&opts)
	// if err != nil {
	// 	log.Fatalf("%s. Cannot check if another watcher is running: %s", meth, err.Error())
	// }
	// if running {
	// 	log.Infof("%s. The watcher is already running. Exiting", meth)
	// 	os.Exit(0)
	// }

	// Creates the .watcher file in the flex volume folder. This file is used to check
	// in the previous step if another watcher is running for this volume
	wpath := path.Join(opts.FlexPath, ".watcher")
	// if volume.FileExists(wpath) {
	// 	log.Infof("%s. The watcher is already running. Exiting", meth)
	// 	os.Exit(0)
	// }
	// err = touchFile(wpath)
	// if err != nil {
	// 	log.Fatalf("%s. Fatal error: %s", meth, err.Error())
	// }
	removeOnExit(wpath)
	defer os.Remove(wpath)

	// Starts watching for changes.
	// err = volume.Watch(&opts)
	err := volume.Watch(&opts)
	for err != nil && volume.DirectoryDeviceChanged(err) {
		err = volume.Watch(&opts)
	}
	if err != nil {
		log.Errorf("%s. Error watching: %s", meth, err.Error())
	}
}

// alreadyRunning checks if there is another watcher running for this volume. The check consists
// in looking for a `.watcher` file in this volume folder.
// func alreadyRunning(opts *volume.WatchOptions) (bool, error) {
// 	watcherFile := path.Join(opts.FlexPath, ".watcher")
// 	if volume.FileExists(watcherFile) {
// 		return true, nil
// 	}

// 	return false, nil
// }

// func touchFile(filepath string) error {
// 	meth := "touchFile"
// 	log.Debugf("%s. Touching file %s", meth, filepath)
// 	wfile, err := os.Create(filepath)
// 	if err != nil {
// 		return err
// 	}
// 	log.Debugf("%s. File %s created", meth, filepath)
// 	defer wfile.Close()
// 	return nil
// }

func removeOnExit(filepath string) {
	meth := fmt.Sprintf("Watcher.removeOnExit. Filepath: '%s'", filepath)
	log.Debugf(meth)
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, syscall.SIGABRT)
	go func() {
		sig := <-sigs
		log.Infof("%s. Signal received: %s. Removing .watcher (if exists)", meth, sig.String())
		if volume.FileExists(filepath) {
			os.Remove(filepath)
		}
		os.Exit(0)
	}()
}
