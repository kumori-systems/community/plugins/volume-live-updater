/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	volume "kuvolume/pkg/volume"

	log "github.com/sirupsen/logrus"
)

// Status is the response status
type Status string

const (
	// StatusSuccess is returned wen everything whent ok
	StatusSuccess Status = "Success"
	// StatusFailure is returned went everything went wrong
	StatusFailure Status = "Failure"
	// StatusNotSupported is returned when the requested operation is not supported
	StatusNotSupported Status = "Not supported"
)

// Capabilities indicates which operations the plugin support
type Capabilities struct {
	Attach bool `json:"attach"`
}

// Output is a plugin response
type Output struct {
	Status       Status        `json:"status"`
	Message      *string       `json:"message,omitempty"`
	Capabilities *Capabilities `json:"capabilities,omitempty"`
}

func init() {
	var writers io.Writer
	meth := "init"

	// Gets the current folder
	// dir, err := filepath.Abs(filepath.Dir(os.Args[0]))

	// Gets the configuration
	config := volume.GetConfig()

	// Opens the log file
	logfilepath := config.PluginLogFile
	file, err := os.OpenFile(logfilepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		fmt.Printf("%s. Error creating log file: %s\n", meth, err.Error())
		// writers = io.MultiWriter(os.Stdout)
	} else {
		writers = io.MultiWriter(file)
	}

	// Configures the log
	// log.SetFormatter(&log.TextFormatter{
	// 	ForceColors: true,
	// })
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true, ForceColors: true})
	log.SetOutput(writers)
	log.SetLevel(config.LogLevel)
	log.Debug(meth)
}

func main() {
	meth := "main"
	log.Debugf("%s", meth)
	if len(os.Args) <= 1 {
		fmt.Printf("{\"status\":\"Failure\", \"message\":\"Expected 2 arguments but found %d\"}", len(os.Args)-1)
		log.Errorf("%s. Wrong arguments %v", meth, os.Args[1:])
		os.Exit(1)
	}

	operation := os.Args[1]
	log.Debugf("%s. Operation: %s. Args: %v", meth, operation, os.Args)

	plugin := volume.Plugin{
		Kubeconfig: "",
	}

	switch operation {
	case "init":
		sendCapabilities()
	case "attach", "detach", "waitforattach", "isattached", "mountdevice", "unmountdevice":
		sendNotSuppored()
	case "mount":
		if len(os.Args) <= 3 {
			message := "Path or json options required but not found"
			log.Errorf("%s Mount failed. %s", meth, message)
			sendFailure(message)
			os.Exit(1)
		}
		filepath := os.Args[2]
		log.Infof("%s. Mounting %s", meth, filepath)
		jsonOptions := os.Args[3]
		var opts volume.MountOptions
		if err := json.Unmarshal([]byte(jsonOptions), &opts); err != nil {
			message := fmt.Sprintf("Cannot unmarshal options: %s", err.Error())
			log.Errorf("%s Mount failed. %s", meth, message)
			sendFailure(fmt.Sprintf(message))
			os.Exit(1)
		}
		var sourcePlugin string
		if opts.VolumePlugin == nil {
			sourcePlugin = "kubernetes.io~configmap"
			opts.VolumePlugin = &sourcePlugin
		}
		if err := plugin.Mount(filepath, opts); err != nil {
			message := fmt.Sprintf("Cannot mount: %s", err.Error())
			log.Errorf("%s Mount failed. %s", meth, message)
			sendFailure(fmt.Sprintf(message))
			os.Exit(1)
		}
		log.Infof("%s. Mounted %s", meth, filepath)
		sendSuccess()

	case "unmount":
		if len(os.Args) <= 2 {
			message := "Path required but not found"
			log.Errorf("%s Unmount failed. %s", meth, message)
			sendFailure(message)
			os.Exit(1)
		}
		filepath := os.Args[2]
		log.Infof("%s. Unmounting %s", meth, filepath)
		if err := plugin.Unmount(filepath); err != nil {
			message := fmt.Sprintf("Cannot unmount: %s", err.Error())
			log.Errorf("%s Unmount failed. %s", meth, message)
			sendFailure(fmt.Sprintf(message))
			os.Exit(1)
		}
		log.Infof("%s. Unmounted %s", meth, filepath)
		sendSuccess()
	default:
		message := fmt.Sprintf("Unknown operation %s", operation)
		log.Errorf("%s. %s", meth, message)
		sendFailure(fmt.Sprintf(message))
		os.Exit(1)
	}
}

func sendCapabilities() {
	response := Output{
		Status: StatusSuccess,
		Capabilities: &Capabilities{
			Attach: false,
		},
	}
	sendOutput((&response))
}

func sendNotSuppored() {
	response := Output{
		Status: StatusNotSupported,
	}
	sendOutput(&response)
}

func sendFailure(message string) {
	response := Output{
		Status:  StatusFailure,
		Message: &message,
	}
	sendOutput(&response)
}

func sendSuccess() {
	response := Output{
		Status: StatusSuccess,
	}
	sendOutput(&response)
}

func sendOutput(output *Output) {
	responseMessage, _ := json.Marshal(*output)
	fmt.Printf(string(responseMessage))
}
