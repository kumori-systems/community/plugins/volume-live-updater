# Copyright 2022 Kumori Systems S.L.
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.

# Image URL to use all building/pushing image targets
# Ticket1279: Adapt kuvolume (flexvolume) to Kubernetes v1.25
# Ticket1359: remove .watcher folder if exists in a mounted volume
VERSION ?= 1.0.0--ticket1279-1359

print-version:
	@echo ${VERSION}


# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

# # Run tests
# test: fmt vet
# 	go test ./... -coverprofile cover.out

# test2: build
# 	./bin/kuvolume mount ${PWD}/tmp/patata "{\"configmap\":\"kd-113152-b3d82209-frontend-configmap\",\"namespace\":\"kumori\",\"key\":\"c1f0\"}"

# testupdater:
# 	./bin/kupdater "{\"sourchepath\":\"/sourcepatj\",\"destpath\":\"destpath\",\"flexpath\":\"flexpath\",\"files\":\"c1f0\",\"indexes\":\"0\"}"

# Builds both binaries
build: fmt vet build-volume build-updater

# Builds both binaries runnable from nodes
nodebuild: fmt vet nodebuild-volume nodebuild-updater

# Builds the volume driver binary
build-volume:
	go build -o bin/kuvolume cmd/volume/main.go

# Creates the volume driver binary runnable from nodes
nodebuild-volume:
	GOARCH=386 GOOS=linux go build -o bin/kuvolume-node cmd/volume/main.go

# Build updater binary
build-updater:
	go build -o bin/kupdater cmd/updater/main.go

# Creates the volumeupdater binary runnable from nodes
nodebuild-updater:
	GOARCH=386 GOOS=linux go build -o bin/kupdater-node cmd/updater/main.go

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...
