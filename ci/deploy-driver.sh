#!/bin/sh

# ORIGINAL SOURCE: https://github.com/kubernetes/examples/tree/master/staging/volumes/flexvolume/deploy

set -o errexit
set -o pipefail


# Set driver vendor and name
VENDOR=${VENDOR:-kumori}
DRIVER=${DRIVER:-kuvolume}


# Prepare destination directory for the driver files
driver_dir=$VENDOR${VENDOR:+"~"}${DRIVER}
if [ ! -d "/flexmnt/$driver_dir" ]; then
  mkdir "/flexmnt/$driver_dir"
fi


# Files are assumed to be in the 'driver_files' directory inside the DaemonSet image.
cp -rf /driver-files/* "/flexmnt/$driver_dir"

while : ; do
  sleep 3600
done
