/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package volume

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"syscall"
	"time"

	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
)

// Watch looks for changes in a source configMap or secret volume folder and reflects
// them in the corresponding subPaths folders.
func Watch(opts *WatchOptions) error {
	meth := fmt.Sprintf("Watcher.Watch. SrcPath: %s DstPath: %s, Files: %v, Indexes: %v", opts.SourcePath, opts.DestPath, opts.MountedFilesNames, opts.MountedFilesIndexes)
	log.Debugf(meth)

	// Check first if the folder exists. We search for the "..data" subfolder (is a soft link
	// to a subfolder) because, otherwise, some times the watcher gets stuck if it is launched
	// before the secret or configMap volume is filled.
	dataSourcePath := fmt.Sprintf("%s/..data", opts.SourcePath)
	if !DirExists(dataSourcePath, 5) {
		log.Errorf("%s. Source folder %s not found. Exiting", meth, opts.SourcePath)
		return fmt.Errorf("Source folder %s not found", opts.SourcePath)
	}

	// Gets the configuration
	config := GetConfig()

	// Gets information about the source path. This is used to detect if the device mounted in
	// this path changes (kubernetes exposes secrets using tmpfs devices)
	sourcePathInfo, err := os.Lstat(opts.SourcePath)
	if err != nil {
		log.Errorf("%s. Error: %v", meth, err)
	}
	sourcePathDev := sourcePathInfo.Sys().(*syscall.Stat_t).Dev
	log.Debugf("%s. Device: %d", meth, sourcePathDev)

	// Runs the file watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	defer watcher.Close()

	// Looks for updates until the source path disappears (the source volume is unmounted).
	done := make(chan bool)
	go func() {

		// Update firs, to ensure the destination path has the correct version
		// if !DirExists(opts.SourcePath, 3) {
		// 	log.Errorf("%s. Source folder %s not found. Exiting", meth, opts.SourcePath)
		// 	done <- true
		// 	return
		// }
		log.Debugf("%s. Updating from %s to %s", meth, opts.SourcePath, opts.DestPath)
		update(opts)

		for {
			log.Debugf("%s. Waiting for watcher events from %s", meth, opts.SourcePath)
			select {
			case event, ok := <-watcher.Events:
				log.Debugf("%s. Sourcepath: %s. Event related to %s: %d", meth, opts.SourcePath, event.Name, uint32(event.Op))
				if !ok {
					log.Errorf("%s. Watcher closed channel. Exiting", meth)
					done <- true
					return
				}
				log.Debugf("%s. Event: %v", meth, event)

				// If the source path folder is no longer a volume folder, exit
				if !DirExists(dataSourcePath, 0) {
					log.Debugf("%s. Source folder %s not found. Exiting", meth, opts.SourcePath)
					done <- true
					return
				}

				// If the source path folder device changed, exit
				currentSourcePathInfo, err := os.Lstat(opts.SourcePath)
				if err != nil {
					log.Errorf("%s. Error checking the source path device: %v. Exiting", meth, err)
					done <- true
				}
				currentSourcePathDev := currentSourcePathInfo.Sys().(*syscall.Stat_t).Dev
				if sourcePathDev != currentSourcePathDev {
					log.Debugf("%s. Device changed from %d to %d. Exiting", meth, sourcePathDev, currentSourcePathDev)
					done <- true
				}

				// After some tests, the events received when a file is updated are: CREATE, CHMOD, RENAME, CREATE, REMOVE.
				// Waits to the final REMOVE event to copy the file just once.
				if event.Op == fsnotify.Remove {
					log.Infof("%s. Updating from %s to %s", meth, opts.SourcePath, opts.DestPath)
					update(opts)
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					log.Errorf("%s. Watcher closed channel. Exiting", meth)
					done <- true
					return
				}
				log.Errorf("%s. Error: %s", meth, err.Error())
			}
		}
	}()

	// Starts a ticker to check regularly if the source path device has changed. If so,
	// the watcher exits to start all over again.
	ticker := time.NewTicker(time.Duration(config.CheckDevicesInterval) * time.Second)
	go func() {
		log.Debugf("%s. Ticker started", meth)
		for {
			select {
			case _, ok := <-ticker.C:
				// Checks if the ticker channel has been closed for whatever reason
				if !ok {
					log.Errorf("%s. Ticker channel closed. Exiting", meth)
					done <- true
					return
				}

				// If the source path folder is no longer a volume folder, exit
				if !DirExists(dataSourcePath, 0) {
					log.Infof("%s. Ticker. Source folder %s not found. Exiting", meth, opts.SourcePath)
					done <- true
					return
				}

				// Checks if the source folder device has changed (if exists)
				currentSourcePathInfo, err := os.Lstat(opts.SourcePath)
				if err != nil {
					log.Errorf("%s. Ticker. Error checking the source path device: %v. Exiting", meth, err)
					done <- true
				}
				currentSourcePathDev := currentSourcePathInfo.Sys().(*syscall.Stat_t).Dev
				if sourcePathDev != currentSourcePathDev {
					log.Infof("%s. Ticker. Device changed from %d to %d. Exiting", meth, sourcePathDev, currentSourcePathDev)
					done <- true
				}
			}
		}
	}()

	// Starts the watcher
	log.Infof("%s. Watching: %s", meth, opts.SourcePath)
	err = watcher.Add(opts.SourcePath)
	if err != nil {
		log.Errorf("%s. Error watching %s: %s", meth, opts.SourcePath, err.Error())
		return err
	}

	// Waits until the source folder is removed or its underlying device changes
	<-done
	log.Debugf("%s. Stop Watching: %s", meth, opts.SourcePath)
	ticker.Stop()
	log.Debugf("%s. Ticker stopped", meth)

	// Checks if the source directory device has chancged. If so, maybe represents an undectected
	// content changed.
	if DirExists(opts.SourcePath, 0) {
		currentSourcePathInfo, err := os.Lstat(opts.SourcePath)
		if err != nil {
			return nil
		}
		currentSourcePathDev := currentSourcePathInfo.Sys().(*syscall.Stat_t).Dev
		if sourcePathDev != currentSourcePathDev {
			return &DirDeviceChangedError{dirpath: opts.SourcePath}
		}
	}

	return nil
}

// update will reflect the content of the files in sourcepath (the source volume path) to
// the destination path (the path storing the copy generated due to the subPath use in the
// volume mount declaration)
func update(opts *WatchOptions) error {
	meth := "Watcher.update"
	log.Debugf(meth)
	sourceRealPath, err := getFilesRealPath(opts)
	if err != nil {
		err := fmt.Errorf("Source path %s not found. Original error: %s", opts.SourcePath, err.Error())
		log.Errorf("%s. %s", meth, err.Error())
		return err
	}
	log.Debugf("%s. SourceRealPath: %s", meth, sourceRealPath)
	mountedFilesNames := strings.Split(opts.MountedFilesNames, ",")
	mountedFilesIndexes := strings.Split(opts.MountedFilesIndexes, ",")
	for i, fileName := range mountedFilesNames {
		sourcePath := path.Join(sourceRealPath, fileName)
		fileIndex := mountedFilesIndexes[i]
		destPath := path.Join(opts.DestPath, fileIndex)
		err = copyFile(sourcePath, destPath)
		if err != nil {
			if os.IsNotExist(err) {
				log.Warnf("%s. File %s not exists. Maybe it has not been created yet. Skipping", meth, sourcePath)
			} else {
				log.Errorf("%s. Error copying file %s to %s: %s", meth, sourcePath, destPath, err.Error())
			}
		}
	}
	return nil
}

// copyFile copies the file content form one path to another
func copyFile(srcFilePath, dstFilePath string) error {
	meth := "Watcher.copyFile"
	log.Debugf("%s. SrcFilePath: %s. DstFilePath: %s.", meth, srcFilePath, dstFilePath)

	srcFile, err := os.Open(srcFilePath)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	dstFile, err := os.Create(dstFilePath)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	bytesWritten, err := io.Copy(dstFile, srcFile)
	if err != nil {
		return err
	}

	log.Debugf("%s. Copied %s to %s. Bytes Written: %d\n", meth, srcFilePath, dstFilePath, bytesWritten)
	return nil
}

// getFilesRealPath looks for the folder storing the real files we are looking
// for. When a configMap or secret file is mounted, internally each declare item
// is stored as a soft link pointing to another soft link in a subpath which at the
// end points to the real file in another subpath (that's the way configMap and secret
// volume plugin work). This functions returns the path storing the real files inside
// the volume. For example, if the source path is
//
//	/var/lib/kubelet/pods/33b43a9e-43b3-44a1-a362-b02c7c82795a/volumes/kubernetes.io~configmap/test-volume-one
//
// That path content might look like this:.
//
//	drwxr-xr-x 2 root root  100 Oct 13 21:07 ..2020_10_13_19_07_50.364129669
//	lrwxrwxrwx 1 root root   31 Oct 13 21:07 ..data -> ..2020_10_13_19_07_50.364129669
//	lrwxrwxrwx 1 root root   13 Oct 13 21:07 file -> ..data/file
//
// And this function will return:
//
//	/var/lib/kubelet/pods/33b43a9e-43b3-44a1-a362-b02c7c82795a/volumes/kubernetes.io~configmap/test-volume-one/..2020_10_13_19_07_50.364129669
func getFilesRealPath(opts *WatchOptions) (string, error) {
	fileInfos, err := ioutil.ReadDir(opts.SourcePath)
	if err != nil {
		return "", err
	}

	var sourceRealPath string
	for _, fileInfo := range fileInfos {
		if !fileInfo.IsDir() {
			continue
		}
		fileName := fileInfo.Name()
		if !strings.HasPrefix(fileName, "..") {
			continue
		}
		if strings.HasPrefix(fileName, "..data") {
			continue
		}
		sourceRealPath = fileName
		break
	}
	if sourceRealPath == "" {
		err := fmt.Errorf("Real path not found for volume %s", opts.SourcePath)
		return "", err
	}

	return fmt.Sprintf("%s/%s", opts.SourcePath, sourceRealPath), nil
}
