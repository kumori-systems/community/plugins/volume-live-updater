/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package volume

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// DefaultCurrentDir is the default folder used if we cannot get the current folder
const DefaultCurrentDir string = "/usr/libexec/kubernetes/kubelet-plugins/volume/exec/kumori~kuvolume/"

// GetCurrentDir returns the directory containing the plugin
func GetCurrentDir() (string, error) {
	return filepath.Abs(filepath.Dir(os.Args[0]))
}

// GetConfig returns the plugin configuration
func GetConfig() *Configuration {

	meth := "volume.GetConfig"

	if currentDir, err := GetCurrentDir(); err == nil {
		log.Debugf("%s. Loading configuration from %s", meth, currentDir)
		viper.AddConfigPath(currentDir)
	} else {
		log.Errorf("%s. Error: %s", meth, err.Error())
		viper.AddConfigPath(DefaultCurrentDir)
	}

	// Sets defaults
	viper.SetDefault("loglevel", "info")
	viper.SetDefault("pluginLogFile", "/var/log/kumori-flexvol-plugin.log")
	viper.SetDefault("updaterLogFile", "/var/log/kumori-flexvol-updater.log")
	viper.SetDefault("checkDevicesInterval", 120)

	// Sets the configuration file name and format
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	// Reads the configuration file
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Warnf("%s. Config file not found", meth)
		} else {
			log.Warnf("%s. Error reading configuration file: %s", meth, err.Error())
		}
	}
	// Calculates the log level
	sLogLevel := strings.ToLower(viper.GetString("loglevel"))
	var logLevel log.Level
	switch sLogLevel {
	case "debug":
		logLevel = log.DebugLevel
	case "info":
		logLevel = log.InfoLevel
	case "warn", "warning":
		logLevel = log.WarnLevel
	case "err", "error":
		logLevel = log.ErrorLevel
	default:
		log.Warnf("%s. Unknown log level '%s'. Setting 'info' by default", meth, sLogLevel)
		logLevel = log.InfoLevel
	}

	sPluginLogPath := viper.GetString("pluginLogFile")
	sUpdaterLogPath := viper.GetString("updaterLogFile")
	iCheckDevicesInterval := viper.GetInt32("checkDevicesInterval")

	// Creates the configuration object
	config := Configuration{
		LogLevel:             logLevel,
		PluginLogFile:        sPluginLogPath,
		UpdaterLogFile:       sUpdaterLogPath,
		CheckDevicesInterval: iCheckDevicesInterval,
	}
	content, _ := json.MarshalIndent(config, "", "  ")
	log.Debugf("%s. Configuration loaded:\n%s\n", meth, content)

	// Returns configuration
	return &config
}
