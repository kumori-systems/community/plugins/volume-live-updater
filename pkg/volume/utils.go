/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package volume

import (
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

// DirExists returns true if the given directory exists and false otherwise
func DirExists(dirpath string, retries int) bool {
	meth := "DirExists"
	info, err := os.Stat(dirpath)
	for i := 0; i < retries; i++ {
		if err == nil {
			break
		}
		if os.IsNotExist(err) {
			log.Infof("%s. Folder %s not exists. Retrying", meth, dirpath)
		} else {
			log.Errorf("%s. Failed accessing %s: %s. Retrying", meth, dirpath, err.Error())
		}
		time.Sleep(100 * time.Millisecond)
		info, err = os.Stat(dirpath)
	}
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// FileExists returns true if the given file exists and false otherwise
func FileExists(filepath string) bool {
	info, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirectoryDeviceChanged checks if this error indicates that a the source path
// device has changed
func DirectoryDeviceChanged(err error) bool {
	switch err.(type) {
	case *DirDeviceChangedError:
		return true
	default:
		return false
	}
}
