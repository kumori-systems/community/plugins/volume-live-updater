/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package volume

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

// MountOptions contains the options needed to mount the volume
type MountOptions struct {
	// MountedFilesNames .
	MountedFilesNames string `json:"files"`
	// MountedFilesIndexes contains
	MountedFilesIndexes string `json:"indexes"`
	// ContainerName is the name of the container where the files are mounted
	ContainerName string `json:"container"`
	// VolumeName is the name of the volume containing the files
	VolumeName string `json:"volume"`
	// VolumePlugin is the name of the plugin used to mount the files in the host (<vendor>~<driver>).
	// By default is kubernetes.io~configmap
	VolumePlugin *string `json:"plugin,omitempty"`
}

// WatchOptions contains the options needed to watch for changes in a ConfigMap volume
// WARNING: the files in `MountedFiles` must appear in the same order they are mounted in the
// container volumeMount section.
type WatchOptions struct {
	// SourcePath is the path to the source volume we want to look for.
	// Usually, `/var/lib/kubelet/pods/<POD_UID>/volumes/kubernetes.io~configmap/<CF_VOLUME_NAME>`
	SourcePath string `json:"sourcepath"`
	// DestPath is the path with the `subpath` we want to update.
	// Usually, `/var/lib/kubelet/pods/<POD_UID>/volume-subpaths/<CF_VOLUME_NAME>/<CONTAINER_NAME>`
	DestPath string `json:"destpath"`
	// FlexPath is the path where the FlexVolume is mounted
	FlexPath string `json:"flexpath"`
	// MountedFilesNames is a string containing a comma separated list of the file names to be watched
	MountedFilesNames string `json:"files"`
	// MountedFilesIndexes is a string containing a comma separated list of the file indexes in the
	// subpaths section.
	MountedFilesIndexes string `json:"indexes"`
}

// Configuration contains the plugin configuration
type Configuration struct {
	LogLevel             log.Level
	PluginLogFile        string
	UpdaterLogFile       string
	CheckDevicesInterval int32
}

// DirDeviceChangedError is an error produced while a Kuku was created. The
// Message represents the error and the Severity the impact on the Kuku-Element
// processing.
type DirDeviceChangedError struct {
	dirpath string
}

// Error message returns a string indicating the error severity and the parent
// error message.
func (e *DirDeviceChangedError) Error() string {
	return fmt.Sprintf("Directory %s device changed", e.dirpath)
}
