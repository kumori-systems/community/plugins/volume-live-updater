/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package volume

import (
	"encoding/json"
	"os"
	"os/exec"
	"path"
	"strings"

	log "github.com/sirupsen/logrus"
)

// Plugin is used to keep updated the files mounted from a configMap or secret volume using subPath.
type Plugin struct {
	Kubeconfig string
}

// Mount creates the flexvolume folder and runs a watcher to update subpaths from the specified
// configMap or secret volume. The flexvolume folder is provided by the kubelet and is used only to hold
// the watcher lock file to avoid multiples instances watching the same volume. The Mount
// function figures out the congiMap volume folder and its subPath folders using the flexVolume
// path and the information stored in the options.
func (plugin *Plugin) Mount(flexVolumeMountPath string, opts MountOptions) error {
	meth := "Plugin.Mount"
	log.Infof("%s. DummyMountpath: %s Options: %v", meth, flexVolumeMountPath, opts)

	// Creates the flexVolume path, if not exists. This volume is mounted mainly to fire the flexvolume
	// driver but is also currently used to store the watcher lock file.
	if !DirExists(flexVolumeMountPath, 1) {
		log.Infof("%s. Creatig volume dir %s", meth, flexVolumeMountPath)
		os.MkdirAll(flexVolumeMountPath, os.ModePerm|0750)
	}

	// Launches the watcher. The watcher will watch for changes in the files mounted in a
	// container from the specified volume and using subPath
	if err := plugin.launchWatcher(flexVolumeMountPath, &opts); err != nil {
		return err
	}

	log.Infof("%s. %s mounted", meth, flexVolumeMountPath)
	return nil
}

// Unmount removes the flexVolume volume.
func (plugin *Plugin) Unmount(mountpath string) error {

	meth := "Plugin.Unmount"

	log.Infof("%s. Unmounting file %s", meth, mountpath)

	if !DirExists(mountpath, 1) {
		log.Infof("%s. File %s not exists. Nothing to do", meth, mountpath)
		return nil
	}

	if err := os.RemoveAll(mountpath); err != nil {
		return err
	}

	log.Infof("%s. File %s unmounted", meth, mountpath)

	return nil
}

// launchWatcher launches a process to watch for changes in a given files in a given path. If
// a file is updated, the changes are copied to the destination file in the subPath folder.
func (plugin *Plugin) launchWatcher(flexVolumeMountPath string, opts *MountOptions) error {
	meth := "Plugin.launchWatcher"
	log.Infof("%s. FlexVolumeMountPath: %s", meth, flexVolumeMountPath)

	running, err := plugin.alreadyRunning(flexVolumeMountPath)
	if err != nil {
		log.Fatalf("%s. Cannot check if another watcher is running: %s", meth, err.Error())
	}
	if running {
		log.Infof("%s. The watcher is already running. Exiting", meth)
		return nil
		// os.Exit(0)
	}

	wpath := path.Join(flexVolumeMountPath, ".watcher")
	err = plugin.touchFile(wpath)
	if err != nil {
		log.Fatalf("%s. Fatal error: %s", meth, err.Error())
		return err
	}

	index := strings.Index(flexVolumeMountPath, "/kumori~kuvolume")
	volumesDir := flexVolumeMountPath[:index]
	podIndex := index - 8
	podDir := flexVolumeMountPath[:podIndex]
	srcPath := path.Join(volumesDir, *opts.VolumePlugin, opts.VolumeName)
	dstPath := path.Join(podDir, "volume-subpaths", opts.VolumeName, opts.ContainerName)
	watchOpts := WatchOptions{
		SourcePath:          srcPath,
		DestPath:            dstPath,
		FlexPath:            flexVolumeMountPath,
		MountedFilesNames:   opts.MountedFilesNames,
		MountedFilesIndexes: opts.MountedFilesIndexes,
	}

	jsonopts, err := json.Marshal(watchOpts)
	if err != nil {
		return err
	}

	pwdpath, _ := os.Getwd()
	log.Infof("%s. Launching watcher from %s to propagate changes from %s to %s", meth, pwdpath, srcPath, dstPath)
	cmd := exec.Command("/usr/libexec/kubernetes/kubelet-plugins/volume/exec/kumori~kuvolume/kupdater", string(jsonopts))
	if err := cmd.Start(); err != nil {
		return err
	}

	log.Infof("%s. Watcher launched", meth)
	return nil
}

// alreadyRunning checks if there is another watcher running for this volume. The check consists
// in looking for a `.watcher` file in this volume folder.
func (plugin *Plugin) alreadyRunning(flexPath string) (bool, error) {
	meth := "Plugin.alreadyRunning"
	watcherFile := path.Join(flexPath, ".watcher")
	if FileExists(watcherFile) {
		return true, nil
	}
	// Ticket 1359. KuVolume creates a file .watcher per mounted file. If the file already exists, it
	// assumes that this file is being already "watched" by a kupdater. This ".watcher" file is mounted
	// in the same container where the watched file is mounted. In rare occasions and due to an unknown,
	// reason, the kubelet tries to mount the .watcher file and it not exists. In that case, it creates
	// an empty .watcher folder. When the kuvolume tries to create the .watcher file it will fail because
	// a .watcher folder already exists. The following code lines prevent that by removing the .watcher
	// folder if exists.
	if DirExists(watcherFile, 0) {
		log.Warnf("%s. Watcher %s exists but it is a directory. Removing...", meth, watcherFile)
		err := os.RemoveAll(watcherFile)
		if err != nil {
			return false, err
		}
	}

	return false, nil
}

func (plugin *Plugin) touchFile(filepath string) error {
	meth := "Plugin.touchFile"
	log.Infof("%s. Touching file %s", meth, filepath)
	wfile, err := os.Create(filepath)
	if err != nil {
		return err
	}
	log.Infof("%s. File %s created", meth, filepath)
	defer wfile.Close()
	return nil
}
