# Kumori Volume

## Table of Contents

* [Overview](#overview)
* [Compilation](#compilation)
* [Installation](#installation)
* [Usage](#usage)
* [License](#license)

## Overview

Kumori Volume is a [Flexvolume](https://github.com/kubernetes/community/blob/master/contributors/devel/sig-storage/flexvolume.md) used to allow hot updates role instances files mounted from V3Deployment parameters or secret resources. It has been designed to work in a Kumori Platform.

The volume is composed by a couple of command line applications (`volume` and `updater`) and implements the following calls:

* `mount`: called when the flex volume is mounted to a role instance.
* `unmount`: called when the flex volume is removed.

It is used together with [configMap](https://kubernetes.io/docs/concepts/storage/volumes/#configmap) and [secret](https://kubernetes.io/docs/concepts/storage/volumes/#secret) volumes.

## Compilation

The volume plugin can be compiled using the `build` target.

```
$ make build
go fmt ./...
go vet ./...
go build -o bin/kuvolume cmd/volume/main.go
go build -o bin/kupdater cmd/updater/main.go
```

The binaries can be found in the `bin` folder:

```
$ ls -l bin
total 11568
-rwxr-xr-x  1 jbgisber  staff  2954840 12 ago 13:23 kupdater
-rwxr-xr-x  1 jbgisber  staff  2963864 12 ago 13:23 kuvolume
```

The `kuvolume` binary is the volume driver and launches `kupdater`, which is in charge of keeping updated the files mounted from a _ConfigMap_ volume and using the `subPath`.

The compilation process requires golang (version 1.13 or greater) installed. The resulting binaries can be execited in systems like the one used during the compilation process.

There is an alternative target `nodebuild` which compiles the plugin for a `i386` linux based system, which fits with the nodes being currently used in Kumori Platform. This target will creates the binaries `kuvolume-node` and `kupdater-node`:

```
$ make nodebuild
go fmt ./...
go vet ./...
GOARCH=386 GOOS=linux go build -o bin/kuvolume-node cmd/volume/main.go
GOARCH=386 GOOS=linux go build -o bin/kupdater-node cmd/updater/main.go
$
$ ls -l bin
total 22136
-rwxr-xr-x  1 jbgisber  staff  2954840 12 ago 13:23 kupdater
-rwxr-xr-x  1 jbgisber  staff  2689034 12 ago 13:32 kupdater-node
-rwxr-xr-x  1 jbgisber  staff  2963864 12 ago 13:23 kuvolume
-rwxr-xr-x  1 jbgisber  staff  2716817 12 ago 13:32 kuvolume-node
```

We can use the `GOARCH` and `GOOS` environment variables to compile to any other system. For example:

```
$ GOOS=darwin GOARCH=amd64 make build
go fmt ./...
go vet ./...
go build -o bin/kuvolume cmd/volume/main.go
go build -o bin/kupdater cmd/updater/main.go
```

## Installation

The plugin must be installed in the Kubernetes cluster nodes as a [flexvolume](https://github.com/kubernetes/community/blob/master/contributors/devel/sig-storage/flexvolume.md) plugin. There is a [recommended deployment method](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/storage/flexvolume-deployment.md#recommended-driver-deployment-method) which can be followed.

WARNING: There isn't a _DaemonSet_ defined yet to deploy this plugin so, if you follow the recommended deployment method, you must create one by your own. If you are installing an entire Kumori platform, this plugin is installed during the platform cluster creation process.

For testing purposes, you can create the folder `/usr/libexec/kubernetes/kubelet-plugins/volume/exec/kumori~kuvolume/` and copy the binaries as `kuvolume` and `kupdater` (compiled to the cluster nodes system) and as root. The _kubelet_ will load the plugin automatically.

## Usage

The plugin is an executable program and can be executed outside Kumori and Kubernetes. However, it is strongly coupled to the way volumes are processed by the kubelet and is strongly recommended to run it in a Kumori platform nodes.

To use this plugin in a POD, you should:

* Declare a _ConfigMap_ plugin including the files you want to mount individually (i.e, using `subPath`).
* Declare in the containers section a _VolumeMount_ per mounted file, using the corresponding `subPath`.
* Declare a _flexVolume_ volume per container and configMap/secret volume using the `kumori/kuvolume` driver. In the options sections, we should specify the mounted files in the precise same order they have been declared in the `volumeMount` section.
* Declare a mount point in the container _VolumeMount_ section for the _flexVolume_. The only purpose of this is to force the _flexVolume_ plugin execution by the _kubelet_.

For example:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
    - name: testcont
      image: httpd
      volumeMounts:
        - name: test-volume
          mountPath: /test/c1f0
          subPath: c1f0
        - name: test-volume
          mountPath: /test/c1f1
          subPath: c1f1
        - name: kuvolume
          mountPath: /dummy/kuvolume
  volumes:
    - name: test-volume
      configMap:
        name: kd-113152-b3d82209-frontend-configmap
        items:
          - key: c1f0
            path: c1f0
          - key: c1f1
            path: c1f1
    - name: kuvolume
      flexVolume:
        driver: "kumori/kuvolume"
        options:
          container: testcont
          volume: test-volume
          files: "c1f0,c1f0"
          indexes: "0,1
```

Internally, the volume only supports the `init`, `mount` and `unmount` calls.

### The `init` call

Returns the plugin cappabilities:

```json
{
  "status": "Success",
  "capabilities": {
    "attach": false
  }
}
```

### The `mount` call

Enables the hot update process for the speciefied files of the specified volume. Requires a path as a first argument and JSON document containing the options as the second argument.

```
$ kuvolume mount <MOUNT_DIR> <JSON_OPTIONS>
```

The options available are:

```json
{
  "container": "testcont",
  "volume": "test-volume",
  "files": "c1f0, c1f2",
  "indexes": "0,1",
  "plugin": "kubernetes.io/configmap"
}
```

The `container` is the name of the container in which the files are mounted. The `volume` contains the name of the `configMap` or `secret` volume mounting those files. The `files` section contains the names of the volume items to be updated. The `indexes` section contains the position of those files in the `VolumeMount` section for this container.

The `plugin` section is optional and defaults to `kubernetes.io~configmap`. It indicates the type of the source plugin. If a secret volume is used instead to mount the files, the value must be `kubernetes.io~secret`.

For compatibility reasons, the path received as a first argument will be created if it not exists. The _kubelet_ expects a volume there and a volume is a folder in this case.

Internally, the `kuvolume` will launch `kupdater` in its own thread, which expects a marshalled JSON document with the following format:

```json
{
  "sourcepath":"/var/lib/kubelet/pods/33b43a9e-43b3-44a1-a362-b02c7c82795a/volumes/kubernetes.io~configmap/test-volume",
  "destpath":"/var/lib/kubelet/pods/33b43a9e-43b3-44a1-a362-b02c7c82795a/volume-subpaths/test-volume/testcont",
  "flexpath":"/var/lib/kubelet/pods/33b43a9e-43b3-44a1-a362-b02c7c82795a/volumes/kumori~kuvolume/kuvolume",
  "files":"c1f0, c1f2",
  "indexes":"0,1"
}
```

The `kuvolume` will watch for changes in `sourcepath` and copy the files in `files` to `destpath` renamed using the index in `indexes`. The `kupdater` will selfdestroy once `srcpath` is removed.

### The `unmount` call

Removes the folder which means removing the unused path we previously created with the `mount` call.

```
$ kuvolume unmount <MOUNT_DIR>
```

Me resultaría raro porque he llegado a ver cambios en caliente en los secrets pero es lo único que se me ocurre.

### Configuration parameters

The plugin looks for configuration parameters in a `config.yaml` file deployed in the same directory than `kuvolume` and `kupdater` binaries. The following configuration parameters are allowed:

* `loglevel`: defaults to `info`.
* `pluginLogFile`: file where the log lines generated by `kuvolume` will be stored. Defaults to `/tmp/kumori-flexvol-plugin.log`.
* `updaterLogFile`: file where the log lines generated by `kupdater` processes will be stored. Defaults to `/tmp/kumori-flexvol-updater.log`.
* `checkDevicesInterval`: time interval (in seconds) used by the `kupdater` to check the status of the observed path. If the observed path doesn't exist or the underlying device has changed, the watcher is rebooted. Defaults to `120`.

## License

Copyright 2022 Kumori Systems S.L.

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:

https://joinup.ec.europa.eu/software/page/eupl

Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and limitations under the Licence.