# Copyright 2022 Kumori Systems S.L.
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.

################################################################################
##  KUMORI WARNING: This Dockerfile is used by CI processes, so any changes   ##
##                  should make sure the CI pipelines don't break.            ##
################################################################################

################################################################################
##  BUILD-STEP IMAGE - Go 1.12                                                ##
################################################################################

# Build the manager binary
FROM golang:1.19 as builder

# These variables are used by CI to clean up intermediate stage images after
# image generation.
ARG DELETE_LABEL=default
LABEL deletemark="${DELETE_LABEL}"
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace

# Allow access to kumori gitlab
RUN mkdir -p /root/.ssh
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.com 2> /dev/null | tee -a /root/.ssh/known_hosts > /dev/null 2>&1
COPY .gitconfig /root/.gitconfig

# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN GOPRIVATE="gitlab.com/kumori/*,gitlab.com/kumori-systems/*" go mod download

# Copy the go source
COPY cmd/ cmd/
COPY pkg/ pkg/
COPY Makefile Makefile

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o bin/kuvolume cmd/volume/main.go
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o bin/kupdater cmd/updater/main.go

RUN make print-version > version.txt

################################################################################
##  ACTUAL DOCKER IMAGE - KUVOLUME based on Busybox                           ##
################################################################################
FROM busybox
WORKDIR /

# TODO Change to your desired driver.
COPY --from=builder /workspace/bin /driver-files
COPY --from=builder /workspace/version.txt /driver-files/version.txt
COPY ./config/config.json /driver-files/config.json

COPY ./ci/deploy-driver.sh /deploy-driver.sh
CMD /bin/sh /deploy-driver.sh









